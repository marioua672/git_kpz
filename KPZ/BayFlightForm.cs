﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPZ
{
    public partial class BayFlightForm : Form
    {
        public BayFlightForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BuyingTicket.aFullName = textBox1.Text;
            BuyingTicket.aCreditСard = textBox2.Text;
            BuyingTicket.aEmail = textBox3.Text;
            Ticket ticket = new Ticket();
            ticket.Show();
            this.Close();
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
        }
    }
}
