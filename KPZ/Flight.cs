﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ
{
    public class Flight
    {
        private string _aFrom;// місце вильоту
        private string _aTo;// місце посадки
        private DateTime _aDate;// дата вильоту
        private string _aGate;// міце реєстріції
        private string _aNameFlight;// код літака
        private DateTime _aBoardTill;// час реєстрації
        private int _aPrise;// ціна білета
        private int _aOccupiedSeats;// Кількість занятих мість
        private int _aMaxSeats;// загальна кількість місць в літаку


        public string aFrom
        {
            get { return _aFrom; }
            set { _aFrom = value; }
        }
        public string aTo
        {
            get { return _aTo; }
            set { _aTo = value; }
        }
        public DateTime aDate
        {
            get { return _aDate; }
            set { _aDate = value; }
        }
        public string aGate
        {
            get { return _aGate; }
            set { _aGate = value; }
        }
        public string aNameFlight
        {
            get { return _aNameFlight; }
            set { _aNameFlight = value; }
        }
        public DateTime aBoardTill
        {
            get { return _aBoardTill; }
            set { _aBoardTill = value; }
        }
        public int aPrise
        {
            get { return _aPrise; }
            set { _aPrise = value; }
        }
        public int aOccupiedSeats
        {
            get { return _aOccupiedSeats; }
            set { _aOccupiedSeats = value; }
        }
        public int aMaxSeats
        {
            get { return _aMaxSeats; }
            set { _aMaxSeats = value; }
        }

        public Flight() // конструктор без параметрів
        {
            aFrom = "Виліт";
            aTo = "Посадка";
            aDate = new DateTime(2100, 5, 1, 8, 30, 00);
            aGate = "Місце реєстріції";
            aNameFlight = "Код літака";
            aBoardTill = new DateTime(2100, 5, 1, 8, 00, 00);
            aPrise = 1;
            aOccupiedSeats = 0;
            aMaxSeats = 100;
        }

        public Flight(string from, string to, DateTime date, string gate, string nameFlight, DateTime boardTill, int prise, int occupiedSeats, int maxSeats)
        {
            this.aFrom = from;
            this.aTo = to;
            this.aDate = date;
            this.aGate = gate;
            this.aNameFlight = nameFlight;
            this.aBoardTill = boardTill;
            this.aPrise = prise;
            this.aOccupiedSeats = occupiedSeats;
            this.aMaxSeats = maxSeats;
        }
    }
}
