﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPZ
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Flight HotList0 = FlightList.aFlightList[0];
            Flight HotList1 = FlightList.aFlightList[1];
            Flight HotList2 = FlightList.aFlightList[2];

            label5.Text = $"From  {HotList0.aFrom}  To  {HotList0.aTo}  Date  {HotList0.aDate}";
            label6.Text = $"From  {HotList1.aFrom}  To  {HotList1.aTo}  Date  {HotList1.aDate}";
            label7.Text = $"From  {HotList2.aFrom}  To  {HotList2.aTo}  Date  {HotList2.aDate}";
        }

        FlightList ListOfTickets = new FlightList();


        private void button1_Click_1(object sender, EventArgs e)
        {
            // зчитування вхідної інформіції
            string from = comboBox1.Text;
            string to = comboBox2.Text;
            DateTime date = dateTimePicker1.Value;


            // перевіка вхідних даних
            if (from == null || to == null) {
                MessageBox.Show($"Ви не увили місто");
                return;
            }
            
            if (from == to)
            {
                if (from == null)
                {
                    MessageBox.Show($"Ви не увили жодне місто");
                    return;
                }
                MessageBox.Show($"Місце вильоту і місце призначення не можуть співпадати");
                return;
            }
            if (date < DateTime.Now)
            {
                MessageBox.Show($"Нажаль ви не моите заказати білет у минуле :(");
                return;
            }


            // перевірка наявності даного білете
            int IDTicket = ListOfTickets.FlightSearch(from, to, date);
            if (IDTicket >= 0)
            {
                ListOfTickets.MessageBoxBayTicket(IDTicket);
                return;
            }

            MessageBox.Show($"Flight not find sorry ");
        }

        private void button2_Click_1(object sender, EventArgs e)// перевірка вхіднах даних до панелі адміністратора і сам вхід
        {
            string login = "admin";
            string password = "admin";


            string loginInput = textBox1.Text;
            string passwordInput = textBox2.Text;

            if (login != loginInput || password != passwordInput)
            {
                MessageBox.Show("Невірний логін або пароль");
                return;
            }

            AdminForm AF = new AdminForm();
            AF.Show();
        }

        private void label5_Click(object sender, EventArgs e)
        {
            ListOfTickets.MessageBoxBayTicket(0);
        }

        private void label6_Click(object sender, EventArgs e)
        {
            ListOfTickets.MessageBoxBayTicket(1);
        }
        private void label7_Click(object sender, EventArgs e)
        {
            ListOfTickets.MessageBoxBayTicket(2);
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
