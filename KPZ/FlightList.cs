﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPZ
{
    public class FlightList
    {
        private static List<Flight> _aFlightList = new List<Flight>();//  масиву рейсів
        public static List<Flight> aFlightList
        {
            get { return _aFlightList; }
            set { _aFlightList = value; }
        }


        public FlightList() 
        {
            aFlightList.Add(new Flight("Kiev", "Ternopil", new DateTime(2022, 1, 1, 8, 30, 00), "A2", "AN225", new DateTime(2022, 1, 2, 8, 00, 00), 870, 21, 100));
            aFlightList.Add(new Flight("Kiev", "Lviv", new DateTime(2022, 1, 1, 8, 30, 00), "A2", "AN225", new DateTime(2022, 1, 2, 8, 00, 00), 930, 21, 100));
            aFlightList.Add(new Flight("Kiev", "Berlin", new DateTime(2022, 1, 1, 8, 30, 00), "A2", "AN225", new DateTime(2022, 1, 2, 8, 00, 00), 670, 21, 100));
            aFlightList.Add(new Flight("Kiev", "Warsaw", new DateTime(2022, 1, 1, 8, 30, 00), "A2", "AN225", new DateTime(2022, 10, 2, 8, 00, 00), 560, 21, 100));
        }


        public int FlightSearch(string from, string to, DateTime date)        // пошук чи є підходящий нам білет

        {
            for (int i = 0; i < _aFlightList.Count; i++)
            {
                if (_aFlightList[i].aFrom == from && _aFlightList[i].aTo == to && _aFlightList[i].aDate.Day == date.Day && _aFlightList[i].aDate.Month == date.Month)
                {
                    return i; // повиртаємо номер рейсу який нам підходить 
                }
                
            };
            return -1;// Якщо незнайдено рейсів нa дану дату за даним рейсом

        }


        public void MessageBoxBayTicket(int id)// перехід до вікна покупки
        {
            Flight flight = FlightList.aFlightList[id];

            DialogResult dialogResult = MessageBox.Show($"From {flight.aFrom} to {flight.aTo} .Date {flight.aDate.Year}.{flight.aDate.Month}.{flight.aDate.Day} , Time {flight.aDate.Hour}.{flight.aDate.Minute} . Gate {flight.aGate}  . Boart TILL {flight.aBoardTill} . Flight {flight.aNameFlight}", "Flight found", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)// якщо так перехід до вікна воду даних про покупку
            {
                BuyingTicket.aTicket = flight;
                BayFlightForm bff = new BayFlightForm();
                bff.Show();
            }
        }
    }
}
