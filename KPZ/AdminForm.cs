﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPZ
{
    public partial class AdminForm : Form
    {

        public AdminForm()
        {
            InitializeComponent();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            foreach (Flight flight in FlightList.aFlightList)
            {
                dataGridView1.Rows.Add(flight.aFrom, flight.aTo, flight.aDate, flight.aGate, flight.aNameFlight, flight.aBoardTill, flight.aPrise, flight.aOccupiedSeats, flight.aMaxSeats);
            }

        }


        private void button1_Click(object sender, EventArgs e)
        {

            FlightList.aFlightList.Clear();


            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                if ((string)dataGridView1[0, i].Value == null) { break; }
                string _from = (string)dataGridView1[0, i].Value;
                string _to = (string)dataGridView1[1, i].Value;
                DateTime _day = Convert.ToDateTime(dataGridView1[2, i].Value);
                string _gate = (string)dataGridView1[3, i].Value;
                string _nameFl = (string)dataGridView1[4, i].Value;
                DateTime _boartTill = Convert.ToDateTime(dataGridView1[5, i].Value);
                int _prise = Convert.ToInt32(dataGridView1[6, i].Value);
                int _occupiedSeats = Convert.ToInt32(dataGridView1[7, i].Value);
                int _maxSeats = Convert.ToInt32(dataGridView1[8, i].Value);

                FlightList.aFlightList.Add(new Flight(_from, _to, _day, _gate, _nameFl, _boartTill, _prise, _occupiedSeats, _maxSeats));

            }

            button1.BackColor = Color.GreenYellow;

        }

    }
}
