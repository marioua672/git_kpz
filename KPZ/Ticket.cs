﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPZ
{
    public partial class Ticket : Form
    {
        public Ticket()
        {
            InitializeComponent();
        }

        private void Ticket_Load(object sender, EventArgs e)
        {
            
            label1.Text = BuyingTicket.aFullName;
            label2.Text = BuyingTicket.aTicket.aFrom;
            label3.Text = BuyingTicket.aTicket.aTo;
            label4.Text = BuyingTicket.aTicket.aNameFlight;
            label5.Text = $"{BuyingTicket.aTicket.aDate.Day.ToString()}.{BuyingTicket.aTicket.aDate.Month.ToString()}.{BuyingTicket.aTicket.aDate.Year.ToString()}";
            label6.Text = $"{BuyingTicket.aTicket.aDate.Hour.ToString()}:{BuyingTicket.aTicket.aDate.Minute.ToString()}";
            label7.Text = BuyingTicket.aTicket.aGate;
            label8.Text = $"{BuyingTicket.aTicket.aBoardTill.Hour.ToString()}:{BuyingTicket.aTicket.aBoardTill.Minute.ToString()}";
            
        }
    }
}
