﻿using KPZ;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace UnitTestProject
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void FlightSearch_KievFrom_LvivTo_20221183000DateTime_1Returned()
        {
            string from = "Kiev";
            string to = "Lviv"; 
            DateTime date = new DateTime(2022, 1, 1, 8, 30, 00);
            int right = 1;

            FlightList flightList = new FlightList();
            int respond = flightList.FlightSearch(from,to,date);

            Assert.AreEqual(right, respond);
        }


        [TestMethod]
        public void FlightSearch_KievFrom_CairoTo_20221183000DateTime_minus1Returned()
        {

            string from = "Kiev";
            string to = "Cairo";
            DateTime date = new DateTime(2022, 1, 1, 8, 30, 00);
            int right = -1;

            FlightList flightList = new FlightList();
            int respond = flightList.FlightSearch(from, to, date);

            Assert.AreEqual(right, respond);
        }


        [TestMethod]
        public void MessageBoxBayTicket_1id_masegboxShow()
        {
            int id = 1;

            FlightList flightList = new FlightList();
            flightList.MessageBoxBayTicket(id);
        }
    }
}
